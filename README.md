# Introduction 

This repository cotains AI ODV association website.
The website is based on a static site generator [hugo](https://gohugo.io/),
[*dimension*](https://themes.gohugo.io/dimension/) theme 
and [gitlab-ci](https://docs.gitlab.com/ee/ci/yaml/).

# How it works
The repository contains an 
[hugo boilerplate](https://gohugo.io/getting-started/directory-structure/) that
you can clone and edit, each time you will push the changes in the master 
gitlab-ci will build and deploy the website using hugo.
You can develop the website following a standard hugo workflow in your
working copy and when you are ready just run a pull request or push changes 
directly to the master branch.

Gitlab-ci will use a docker container to build the website with hugo and then it 
will sync (using webdav) all the static files with the remote dirctory in the
webserver. This process is described in **.gitlab-ci.yml** file.

